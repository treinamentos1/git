**1. Instrutor**

**2. GIT**
- 2.1. Conceitos de Controle de Versão e Motivação
- 2.4. Estrutura de um Controle de Versão e Diferenças entre GIT e SVN

**3. Começando Simples**
- 3.1 git init
- 3.2 git add
- 3.3 git status
- 3.4 git commit
- 3.5 git pull
- 3.6 git push
- 3.7 git log
- 3.8 git clone
- 3.9 git branch
- 3.10 git merge

**4. Gitlab**
- 4.1. Introdução ao Gitlab
- 4.2 Grupos
- 4.3 Criando um projeto
- 4.4 Permissão em grupos e projeto.
- 4.5. Fork
- 4.6 Merge Request

**5. Melhores Práticas no Controle de Versão**
- 5.1 Workflow Gitflow
- 5.2 Versionamento Semântico

## Instrutor

- Claudio A. Silva
- Analista de sistemas
- Graduação
  - Graduação Análises e desenvolvimento de sistemas
  - Pós em Licenciatura da computação
  - Pós em Devops

- Certificações: 
  - ITIL (Information Technology Infrastructure Library)
  - DevOps Professional
  - LPIC 701 - DevOps Engineer
  - LPIC 2
  - Kanban associate.
  - CTFL (Certified Tester Foundation Level)
  - DCA (Docker Certified Associate)

## 2. Git

O Git foi desenvolvido pelo criador do kernel do Linux, Linus Torvalds, em 2005.

![https://miro.medium.com/v2/resize:fit:606/format:webp/1*L2tw8uFLiRhfK_rGfzbHgg.jpeg](https://miro.medium.com/v2/resize:fit:606/format:webp/1*L2tw8uFLiRhfK_rGfzbHgg.jpeg)

Instalação do Git
[https://git-scm.com/book/pt-br/v2/Come%C3%A7ando-Instalando-o-Git](https://git-scm.com/book/pt-br/v2/Come%C3%A7ando-Instalando-o-Git)

### 2.1. Conceitos de Controle de Versão e Motivação
- Tem a finalidade de gerenciar diferentes versões de um documento;
- Maneira inteligente e eficaz de organizar projetos;
- Acompanhar histórico de desenvolvimento;
- Desenvolver paralelamente;
- Resgatar o projeto em um ponto que estava estável;

Basicamente, os arquivos do projeto ficam armazenados em um repositório. O histórico de suas versões é salvo nele. Os desenvolvedores acessam e resgatam a versão disponível e fazem uma cópia local, na qual poderão desenvolver. Ao efetuar as mudanças necessárias, é possível enviar novamente ao servidor e atualizar a sua versão a partir outras feitas pelos demais desenvolvedores.

![git-concept.jpeg](git-concept.jpeg)


### 2.4. Estrutura de um Controle de Versão e Diferenças entre GIT e SVN


## 3. Começando Simples

### 3.1 **git init**
Este comando cria um novo repositório do Git.
A execução do git init cria um subdiretório .git no diretório de trabalho atual, que contém todos os metadados Git necessários para o novo repositório.

Dois exemplos de criar o repositório. Crie um diretório com nome **meurepo**

Acesse o diretório existente e execute
```
git init
```

Execute o comando independente se existe o diretório, ele será criado na execução.
```
git init meurepo
```

### 3.2 **git add**
Utilizado para adicionar um novo arquivo a ser rastreado no repositório remoto.

Acesse o diretório e crie um arquivo chamado **readme.md** contendo o texto "meu teste 1". Caso utilizar Linux, pode executar o comando:
```
echo "meu teste 1" > readme.md
```

Agora, execute:
```
git add .
```

### 3.3 **git status**
O próprio comando menciona o que faz, verifica o status do nosso repositório local. 
```
git status
```

### 3.4 **git commit**
O comando git commit captura um instantâneo das mudanças preparadas do projeto no momento. temos que adicionar uma mensagem para indicar o que mudou naquele momento do repositório.
```
git commit -m "Meu primeiro commit"
```

### 3.5 **git push**
Este comando envia o commit ao repositório remoto.

Como não temos nenhum repositório criado remotamente, teremos problemas, então vamos criar um com o nome **meurepo** adicionando a URL https://gitlab.com/SEU_USER .

ex:
```
git remote add meurepo https://gitlab.com/claudiosilva/
```

Agora sim, podemos fazer o push.
```
git push https://gitlab.com/claudiosilva/meurepo
```

### 3.6 **git log**
Como sempre o git e sua intuitividade, este comando visualiza os logs dos commits em um repositório.
```
git log
```

### 3.7 **git clone**
Supondo que você já tenha um repositório remoto criado, faremos o clone deste repositório para nossa estação de trabalho.
Faça um teste removendo o diretório **meurepo** e executando o comando abaixo:
```
git clone https://gitlab.com/claudiosilva/meurepo
```

### 3.8 **git pull**
Suponhamos que outro desenvolvedor esteja utilizando a mesma branch que você e alterando arquivos. Para atualizar sua branch local, você utiliza o comando:
```
git pull
```
para exemplo, altere algo no arquivo **readme.md** pelo gitlab, adicione a frase na segunda linha: **meu teste 2**

Agora faça um commit com a mensagem **Meu segundo commit**.

Em seguida execute o pull pela linha de comando, dentro do diretório da sua estação de trabalho.
```
git pull
```

### 3.9 **git branch**
Agora ficou bacana, vamos começarr entender como funciona a criação e mudança de branches no git.

Uma branch é uma ramificação, uma linha do tempo criada no repositório para a mudança.

para ver qual branch estamos executamos:
```
git branch
```

Vimos que estamos na branch **Master** inicialmente. Para criar uma nova branch chamada **develop** à partir dela, executamos o seguinte:
```
git checkout -b develop
```
Faça o git push desta branch, assim que cria-la.
O git é intuitivo e apresenta uma mensagem de alerta. Como esta branch ainda não existe remotamente, precisamos executar.
```
git push --set-upstream origin develop
```

Para sair da branch develop e voltar á master, podemos executar:
```
git checkout master
```

### 3.9 **git merge**

O Merge, junta as mudanças de uma branch em outra. Como exemplo, acesse a branch develop.
Editaremos o arquivo readme.md, criaremos uma nova linha e adicionaremos o texto: **meu teste 3**.
Em seguida vamo seguit o fluxo para enviar a mudança:
```
git add *
git commit -m "Meu terceiro commit"
git push
```

Agora, faremos a mesclagem do código develop para a Master.
Vá para a branch master, e execute o comando para trazer as mudanças da develop para a master:
```
git checkout master
git log
git merge develop
git log
git push
```

# GitLab

## 4.1. Introdução ao Gitlab

Git é considerado o arroz com feijão de projetos de desenvolvimento. Não importa qual seja sua especialidade, você vai precisar dele e é importante que saiba utilizá-lo do jeito certo.

GitLab é uma plataforma de hospedagem de código-fonte. Ela permite que profissionais de desenvolvimento contribuam em projetos privados ou abertos.

O Gitlab teve sua primeira versão disponibilizada em 2011 para concorrer com o Github, até então a plataforma mais bem-sucedida.

Em 2018, a Microsoft adquiriu o Github, diversos usuários descontentes começaram migrar para outras ferramentas, inclusive GitLab.

### 4.2 Grupos
Um grupo é uma forma de melhor organização para um conjunto de projetos, juntamente com dados sobre como os usuários podem acessar esses projetos. Cada grupo tem um espaço para nome de projeto (da mesma forma que os usuários).

Para criar um grupo, clicamos no ícone **+** na barra de ferramentas superior. Será apresentado a opção de criação do grupo.

- Selecione a opção **novo grupo** -> **Criar novo grupo**. Agora Adicione um nome único para seu grupo.
- Selecione o nível de visibilidade **Público**
- Clique em criar grupo.

Á partir daqui, podemos criar novos projetos ou subgrupos dentro deste grupo.

### 4.3 Criando um projeto
Um Projeto é um repositório. Para criar um projeto, clicamos no ícone **+** na barra de ferramentas superior. Será apresentado a opção **Novo projeto/repositório** -> **Criar projeto em brancho**.

Em **Nome do projeto** insiria o nome **projeto1**.

Perceba que temos na opção **URL do projeto**, algumas possibilidades interessantes de onde o projeto vai ficar e como será acessado.

Torne-o Privado.

### 4.4 Permissão em grupos e projeto.

Para tornar um **projeto** privado ou público;
- Menu lateral **Configurações**
- Expandir **Visibilidade, recursos de projeto, permissões**
- Selecione sua opção em **Visibilidade do projeto**.

Para tornar um **grupo** privado ou público;
- Menu lateral **Configurações**
- Em **Nível de visibilidade** podemos escolher entre Privado ou Público.

Para adicionar permissão no **projeto** para um usuário.
- Menu lateral **Informação do projeto** -> **Membros**.
- Clicar no botão **Convidar membros**.
  - Inserir o username ou email.
  - Selecionar um cargo.
  - Clicar em **Convidar**
Após adicionado, acesse com o outro usuário para confirmar permissões, em seguida, remova o acesso.

Para adicionar permissão no **Grupo** para um usuário.
- Menu lateral **Informação do projeto** -> **Membros**.
- Clicar no botão **Convidar membros**.
  - Inserir o username ou email.
  - Selecionar um cargo.
  - Clicar em **Convidar**
À partir deste momento, o usuário convidado terá acesso a todos os projetos/repositórios deste grupo.

### 4.5. Fork
Fork é uma duplicata do repositório original no qual pode ser feito as alterações sem afetar o projeto original.

Com o usuário **duzerubr** acessando a URL [https://gitlab.com/treinamentos1/git](https://gitlab.com/treinamentos1/git) que foi criada pelo usuário **claudiosilva**, aparecerá o botão **Fork**.
Conclua o Fork.

Acessando novamente o repositório https://gitlab.com/treinamentos1/git, veremos que agora teremos um número a mais de fork(s).

### 4.6. Merge Request

No Fork criado com o usuário **duzerubr**, vamos criar um novo arquivo .txt. Agora vamos efetuar a solicitação de merge no repositório original.
- No menu lateral acesse **Solicitações de mesclagem**.
- Em **Ramifiação de origem** Selecione a branch do repositório.
- Em **Ramificação de destino** selecione treinamentos1/git
- Clicar em **Comparar ramificações e continuar**.
- Adicione um título e uma descrição da solicitação de mesclagem do repositório.

À partir daqui, o usuário **claudiosilva** conseguirá ver o merge request, e decidirá se mescla o código.


## 5. Melhores Práticas no Controle de Versão

### 5.1 Workflow Gitflow
- Feature:
  Para Teste de desenvolvimento
  Branch temporária até o desenvolvimento.

- Develop
  Uma cópia da branch principal master, Base para o desenvolvimento de novas features
  Branch fixa

- Release
  Um passo antes de produção. Nela unimos o que está pronto em nossa branch develop e “jogamos” para a branch principal. No mais, é criado uma nova tag no nosso projeto.
  branch temporária

- Hotfix
  Utilizada quando ocorre algum problema no ambiente de produção no qual a correção deve ser feita imediatamente.
  branch temporária

- Master
  Branch em Produção
  Branch fixa

### 5.2 Versionamento Semântico

O padrão do versionamento Semântico é uma sequência numérica separada em três posições:

- **MAJOR** (Maior)
O primeiro dígito informa a versão de compatibilidade e é alterado caso o software ou biblioteca sofra mudanças que a torne incompatível com outras versões. São as chamadas breaking changes, atualizações que possuem o potencial de “quebrar” códigos que utilizam versões anteriores.

    Exemplo: Mudar de Wildfly 21 para Wildfly 23.

    Versão **_1.0.0_** → Agora é **_2.0.0_**

- **MINOR** (Menor)
O segundo dígito informa a versão da funcionalidade, onde uma nova função ou melhoria substancial é adicionada e não há problemas de incompatibilidade com outras versões.

    Exemplo: Uma nova funcionalidade de visualizar contracheque.

    Versão **_2.0.0_** → Agora é **_2.1.0_**

- **PATCH** (Correção)
O terceiro dígito informa a versão da correção de bugs, melhorias de desempenho ou alterações similares que não alteram as funcionalidades atuais e nem introduzem novas.

    Exemplo: A nova funcionalidade de visualizar contracheque tem um bug que gera uma vulnerabilidade no código.

    Versão **_2.1.0_** → Agora é **_2.1.1_**
